Hooks.on("ready", () => {
WFRP4E.species["goth-barbaren"] = "Goth-Barbaren"

WFRP4E.speciesCharacteristics["goth-barbaren"] = {
	"ws" : "2d10+20",
    "bs" : "2d10+20",
    "s"  : "2d10+20",
    "t"  : "2d10+20",
    "i"  : "2d10+20",
    "ag" : "2d10+20",
    "dex": "2d10+20",
    "int": "2d10+20",
    "wp" : "2d10+20",
    "fel": "2d10+20"
}

WFRP4E.speciesSkills["goth-barbaren"] =  [
    "Entertain (Storytelling)",
    "Perception",
    "Climb",
    "Cool",
    "Endurance",
    "Consume Alcohol",
	"Outdoor Survival",
    "Language (Myrtanisch)",
    "Language (Nordmarisch)",
    "Lore (Nordmar)",
    "Melee (Basic)",
    "Navigation"
  ]

 WFRP4E.speciesTalents["goth-barbaren"] = [
    "Berserk Charge, Hardy",
    "Night Vision",
	"Warrior Born",
    1
  ]

 WFRP4E.speciesFate["goth-barbaren"] = 2;

 WFRP4E.speciesRes["goth-barbaren"] = 1;

 WFRP4E.speciesExtra["goth-barbaren"] = 3;

 WFRP4E.speciesMovement["goth-barbaren"] = 4;
 
 /**hashishins*/
 WFRP4E.species["goth-hashishins"] = "Goth-Hashishins"

WFRP4E.speciesCharacteristics["goth-hashishins"] = {
	"ws" : "2d10+20",
    "bs" : "2d10+20",
    "s"  : "2d10+20",
    "t"  : "2d10+20",
    "i"  : "2d10+20",
    "ag" : "2d10+20",
    "dex": "2d10+20",
    "int": "2d10+20",
    "wp" : "2d10+20",
    "fel": "2d10+20"
}

WFRP4E.speciesSkills["goth-hashishins"] =  [
    "Evaluate",
    "Charm",
    "Endurance",
    "Cool",
    "Gossip",
    "Trade ()",
	"Haggle",
    "Language (Myrtanisch)",
    "Language (Varantisch)",
    "Lore (Varant)",
    "Melee (Basic)",
    "Ranged (Bow)"
  ]

 WFRP4E.speciesTalents["goth-hashishins"] = [
    "Ambidextrous, Craftsman",
    2
  ]

 WFRP4E.speciesFate["goth-hashishins"] = 2;

 WFRP4E.speciesRes["goth-hashishins"] = 1;

 WFRP4E.speciesExtra["goth-hashishins"] = 3;

 WFRP4E.speciesMovement["goth-hashishins"] = 4;
 
  /**Nomads*/
WFRP4E.species["goth-nomads"] = "Goth-Nomads"

WFRP4E.speciesCharacteristics["goth-nomads"] = {
	"ws" : "2d10+20",
    "bs" : "2d10+20",
    "s"  : "2d10+20",
    "t"  : "2d10+20",
    "i"  : "2d10+20",
    "ag" : "2d10+20",
    "dex": "2d10+20",
    "int": "2d10+20",
    "wp" : "2d10+20",
    "fel": "2d10+20"
}

WFRP4E.speciesSkills["goth-nomads"] =  [
    "Outdoor Survival",
    "Language (Myrtanisch)",
    "Language (Varantisch)",
    "Track",
    "Leadership",
    "Navigation",
	"Melee (Basic)",
    "Ranged (Bow)",
    "Lore (Adanos)",
    "Gossip",
    "Endurance",
    "Ride (Horse)"
  ]

 WFRP4E.speciesTalents["goth-nomads"] = [
    "Stone Soup",
    "Orientation",
    1
  ]

 WFRP4E.speciesFate["goth-nomads"] = 2;

 WFRP4E.speciesRes["goth-nomads"] = 1;

 WFRP4E.speciesExtra["goth-nomads"] = 3;

 WFRP4E.speciesMovement["goth-nomads"] = 4;
 
   /**Myrtaner*/
WFRP4E.species["goth-myrtaner"] = "Goth-Myrtaner"

WFRP4E.speciesCharacteristics["goth-myrtaner"] = {
	"ws" : "2d10+20",
    "bs" : "2d10+20",
    "s"  : "2d10+20",
    "t"  : "2d10+20",
    "i"  : "2d10+20",
    "ag" : "2d10+20",
    "dex": "2d10+20",
    "int": "2d10+20",
    "wp" : "2d10+20",
    "fel": "2d10+20"
}

WFRP4E.speciesSkills["goth-myrtaner"] =  [
    "Animal Care",
    "Charm",
    "Leadership",
    "Cool",
    "Evaluate",
    "Gossip",
	"Haggle",
    "Consume Alcohol",
    "Language (Myrtanisch)",
    "Lore (Myrtana)",
    "Melee (Basic)",
    "Ranged (Bow)"
  ]

 WFRP4E.speciesTalents["goth-myrtaner"] = [
    "Savvy, Suave",
    3
  ]

 WFRP4E.speciesFate["goth-myrtaner"] = 2;

 WFRP4E.speciesRes["goth-myrtaner"] = 1;

 WFRP4E.speciesExtra["goth-myrtaner"] = 3;

 WFRP4E.speciesMovement["goth-myrtaner"] = 4; 

   /**Waldvolk*/
WFRP4E.species["goth-waldvolk"] = "Goth-Waldvolk"

WFRP4E.speciesCharacteristics["goth-waldvolk"] = {
	"ws" : "2d10+20",
    "bs" : "2d10+20",
    "s"  : "2d10+20",
    "t"  : "2d10+20",
    "i"  : "2d10+20",
    "ag" : "2d10+20",
    "dex": "2d10+20",
    "int": "2d10+20",
    "wp" : "2d10+20",
    "fel": "2d10+20"
}

WFRP4E.speciesSkills["goth-waldvolk"] =  [
    "Athletics",
    "Climb",
    "Endurance",
    "Animal Charm",
    "Language (Myrtanisch)",
    "Melee (Basic)",
	"Ranged (Bow)",
    "Stealth (Rural)",
    "Track",
    "Outdoor Survival",
    "Perception",
    "Animal Care"
  ]

 WFRP4E.speciesTalents["goth-waldvolk"] = [
    "Suave (Sight)",
    "Flee!",
    1
  ]

 WFRP4E.speciesFate["goth-waldvolk"] = 2;

 WFRP4E.speciesRes["goth-waldvolk"] = 1;

 WFRP4E.speciesExtra["goth-waldvolk"] = 3;

 WFRP4E.speciesMovement["goth-waldvolk"] = 4;

   /**Gnome*/ 
WFRP4E.species["goth-gnome"] = "Goth-Gnome"

WFRP4E.speciesCharacteristics["goth-gnome"] = {
	"ws" : "2d10+20",
    "bs" : "2d10+30",
    "s"  : "2d10+10",
    "t"  : "2d10+20",
    "i"  : "2d10+30",
    "ag" : "2d10+30",
    "dex": "2d10+30",
    "int": "2d10+30",
    "wp" : "2d10+40",
    "fel": "2d10+30"
}

WFRP4E.speciesSkills["goth-gnome"] =  [
    "Stealth ()",
    "Sleight of Hand",
    "Charm",
    "Dodge",
    "Intuition",
    "Trade (Cook)",
	"Entertain ()",
    "Language (Gnome)",
    "Language (any Human)",
    "Melee (Basic)",
    "Ranged (Bow)",
    "Lore (Geology)"
  ]

 WFRP4E.speciesTalents["goth-gnome"] = [
    "Acute Sense (Smell)",
    "Small",
    "Night Vision",
    2
  ]

 WFRP4E.speciesFate["goth-gnome"] = 0;

 WFRP4E.speciesRes["goth-gnome"] = 2;

 WFRP4E.speciesExtra["goth-gnome"] = 3;

 WFRP4E.speciesMovement["goth-gnome"] = 3;
 
    /**Draconians*/ 
WFRP4E.species["goth-draconians"] = "Goth-Draconians"

WFRP4E.speciesCharacteristics["goth-draconians"] = {
	"ws" : "2d10+30",
    "bs" : "2d10+20",
    "s"  : "2d10+20",
    "t"  : "2d10+30",
    "i"  : "2d10+20",
    "ag" : "2d10+30",
    "dex": "2d10+10",
    "int": "2d10+20",
    "wp" : "2d10+40",
    "fel": "2d10+10"
}

WFRP4E.speciesSkills["goth-draconians"] =  [
    "Language (Draconic)",
    "Language (any Human)",
    "Melee (Basic)",
    "Ranged (Bow)",
    "Swim",
    "Lore (Beliar)",
	"Endurance",
    "Climb",
    "Athletics",
    "Track",
    "Perception",
    "Animal Care"
  ]

 WFRP4E.speciesTalents["goth-draconians"] = [
    "Magic Resistance",
    "Scale Sheer Surface",
	"Acute Sense (Smell)",
    0
  ]

 WFRP4E.speciesFate["goth-draconians"] = 0;

 WFRP4E.speciesRes["goth-draconians"] = 2;

 WFRP4E.speciesExtra["goth-draconians"] = 2;

 WFRP4E.speciesMovement["goth-draconians"] = 5; 
})